package org.innopolis.kuzymvas.unicomparable;

import java.util.Objects;

/**
 * Иммутабельный контейнер, хранящий ссылку на некий объект и сопоставленный ему токен сравнения
 */
public class UniComparableContainer implements UniComparable{

    private final UniComparableToken token; // хранимый токен
    private final Object value; // исходный объект

    public UniComparableContainer(Object value) {
        token = new UniComparableToken();
        this.value = value;
    }

    public Object getValue() {
        return  value;
    }

    @Override
    public UniComparableToken getComparableToken() {
        return token;
    }

    @Override
    public String toString() {
        return "UCC: " +
                value;
    }


}
