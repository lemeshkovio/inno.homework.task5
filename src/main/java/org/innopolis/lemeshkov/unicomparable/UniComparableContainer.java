package org.innopolis.lemeshkov.unicomparable;

/**
 * Иммутабельный контейнер, хранящий ссылку на некий объект и сопоставленный ему токен сравнения
 */
public class UniComparableContainer implements UniComparable{

    private final UniComparableToken token; // хранимый токен
    private final Object value; // исходный объект

    public UniComparableContainer(Object value) {
        token = new UniComparableToken();
        this.value = value;
    }

    public Object getValue() {
        return  value;
    }

    @Override
    public UniComparableToken getComparableToken() {
        return token;
    }

    @Override
    public String toString() {
        return "UCC: " +
                value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UniComparableContainer other = (UniComparableContainer) obj;
        if (token == null) {
            if (other.token != null)
                return false;
        } else if (!token.equals(other.token))
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((token == null) ? 0 : token.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }
}
