package org.innopolis.lemeshkov.my_hashmap;

import org.innopolis.lemeshkov.data_unit.KeyValuePair;
import org.innopolis.lemeshkov.exceptions.KeyNotPresentException;

public interface MyHashMap <K, V>{

    /**
     * Places a key-value pair in storage. If key is already exist in store, replaces its value with the value specified in the argument.    
     * @param key - key to add
     * @param value - value to add
     */
    void put(K key, V value);

    /**
     * Replaces value of given key with given value. Throws an exception if given key does not exist    
     * @param key - key for which the replacement is performed
     * @param value - value to replace the current one
     * @throws KeyNotPresentException - thrown when there is no given key
     */
    void replace(K key, V value) throws KeyNotPresentException;

    /**
     * Returns the value associated with the given key. Throws an exception if the specified key does not exist
     * @param key - key for target value
     * @return - value associated with the given key
     * @throws KeyNotPresentException - thrown when the specified key does not exist
     */
    Object get(K key) throws KeyNotPresentException;

    /**
     * Removes a key-value pair with the given key from storage. Throws an exception if the specified key does not exist  
     * @param key - key for target value
     * @return - the value associated with given key
     * @throws KeyNotPresentException - thrown when the specified key does not exist
     */
    Object remove(K key) throws KeyNotPresentException;

    /**
     * Checks for existing of the given key
     * @param key - key to check
     * @return - true if key is exist, false else
     */
    boolean containsKey(K key);

    /**
     * Checks for existing of the given key-value pair
     * @param pair - pair to check
     * @return - true, if pair exist, false else
     */
    boolean containsPair(KeyValuePair<K, V> pair);

    /**
     * Returns the count of key-value pairs in the storege
     * @return - count of key-value pairs.
     */
    int size();

}
