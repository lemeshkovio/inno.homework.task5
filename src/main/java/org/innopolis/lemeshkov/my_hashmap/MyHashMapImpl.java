package org.innopolis.lemeshkov.my_hashmap;

import java.util.Arrays;

import org.innopolis.lemeshkov.data_unit.KeyValuePair;
import org.innopolis.lemeshkov.data_unit.Node;
import org.innopolis.lemeshkov.exceptions.KeyNotPresentException;

public class MyHashMapImpl<K, V> implements MyHashMap<K, V> {

    final Node<K, V>[] buckets;
    int size;

    public MyHashMapImpl() {
        this(1024);
    }

    /**
     * Create new HashMap with buckets count
     *
     * @param bucketsCount
     *            - buckets count
     * @throws IllegalArgumentException
     *             - throws if buckets count <= 0.
     */
    public MyHashMapImpl(int bucketsCount) throws IllegalArgumentException {
        size = 0;
        if (bucketsCount <= 0)
            throw new IllegalArgumentException("Hash map can't have 0 or less buckets");
        buckets = new Node[bucketsCount];
    }

    @Override
    public void put(K key, V value) {
        final int keyHash = getKeyBucket(key);
        if (buckets[keyHash] == null) { 
            buckets[keyHash] = new Node<K, V>(key, value);
            size++;
        } else {
            if (buckets[keyHash].putIntoList(key, value)) {
                size++;
            }
        }

    }

    @Override
    public Object get(K key) throws KeyNotPresentException {
        final int keyHash = getKeyBucket(key);
        if (buckets[keyHash] != null) {
            return buckets[keyHash].getValue(key);
        } else {
            throw new KeyNotPresentException("Key not found in hash map during get attempt. Key value: " + key);
        }

    }

    @Override
    public Object remove(K key) throws KeyNotPresentException {
        final int keyHash = getKeyBucket(key);
        if (buckets[keyHash] != null) {
            V value = (V) buckets[keyHash].getValue(key);
            buckets[keyHash] = buckets[keyHash].removeFromList(key);
            size--;
            return value;
        } else {
            throw new KeyNotPresentException("Key not found in hash map during remove attempt. Key value: " + key);
        }
    }

    @Override
    public boolean containsKey(K key) {
        final int keyHash = getKeyBucket(key);
        return buckets[keyHash] != null && buckets[keyHash].containsKey(key);
    }

    @Override
    public boolean containsPair(KeyValuePair<K, V> pair) {
        final int keyHash = getKeyBucket((K) pair.getKey());
        return buckets[keyHash] != null && buckets[keyHash].containsPair(pair);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public String toString() {
        final StringBuilder string = new StringBuilder("MyHashMap{ size=").append(size).append(", buckets=");
        for (int i = 0; i < buckets.length; i++) {
            if (buckets[i] != null) {
                string.append("bucket[").append(i).append("]{");
                buckets[i].describeList(string);
                string.append((i < buckets.length - 1) ? "}," : "}");
            }
        }
        string.append(" }");
        return string.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof MyHashMap))
            return false;
        final MyHashMap<K, V> that = (MyHashMap<K, V>) o;
        if (this.size != that.size())
            return false;
        for (Node<K, V> bucket : buckets) {
            if (bucket != null) {
                KeyValuePair<K, V>[] pairs = bucket.getKeyValuePairs();
                for (KeyValuePair<K, V> pair : pairs) {
                    if (!that.containsPair(pair)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int[] nodeHashes = new int[size];
        int currPos = 0;
        for (Node<K, V> bucket : buckets) {
            if (bucket != null) {
                int[] bucketHashes = bucket.getKeyValuePairsHashes();
                System.arraycopy(bucketHashes, 0, nodeHashes, currPos, bucketHashes.length);
                currPos += bucketHashes.length;
            }
        }
        Arrays.sort(nodeHashes);
        return Arrays.hashCode(nodeHashes);
    }

    @Override
    public void replace(K key, V value) throws KeyNotPresentException {
        if (!this.containsKey(key)) {
            throw new KeyNotPresentException("Don't contain key");
        } else {
            put(key, value);
        }
    }
    
    /**
     * Specifies the basket number corresponding to the key
     *
     * @param key
     *            - bucket key
     * @return bucket key from map
     */
    private int getKeyBucket(K key) {
        return (key == null) ? 0 : Math.abs(key.hashCode()) % buckets.length;
    }
}
