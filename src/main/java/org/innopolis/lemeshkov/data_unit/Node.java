package org.innopolis.lemeshkov.data_unit;

import java.util.Arrays;

import org.innopolis.lemeshkov.exceptions.KeyNotPresentException;

/**
 * Class to formes key-value pairs list
 */
public class Node <K, V>{

    KeyValuePair<K, V> pair;
    private Node<K, V> next;
    private long height;

    /**
     * Create new Node with key-value pair
     *
     * @param key
     *            - new key
     * @param value
     *            - new value
     */
    public Node(K key, V value) {
        height = 1L;
        this.pair = new KeyValuePair<K, V>(key, value);
    }

    /**
     * If key does not exist in this list, then
     * inserts a new node into this list containing the specified key-value
     * pair. If the given key is already in the list, then replaces the
     * corresponding value.
     *
     * @param key
     *            - key, contained in inserted node
     * @param value
     *            - value, contained in inserted node
     * @return - true, if new Node created, false else
     */
    public boolean putIntoList(K key, V value) {
        if (pair.hasKey(key)) {
            pair = new KeyValuePair<K, V>(key, value);
            return false;
        }
        if (next != null) {
            boolean res = next.putIntoList(key, value);
            if (res) {
                height++;
            }
            return res;
        } else {
            next = new Node<K, V>(key, value);
            height++;
            return true;
        }
    }

    /**
     * Удаляет из списка, начинающегося с данного узла узел с заданным ключем
     *
     * @param key
     *            - ключ удаляемого узла
     * @return - новая голова списка.
     * @throws KeyNotPresentException
     *             - выбрасывается, если в списке нет узла с заданным ключем.
     */
    public Node<K, V> removeFromList(K key) throws KeyNotPresentException {
        if (pair.hasKey(key)) {
            height--;
            return this.next;
        } else {
            if (this.next != null) {
                this.next = this.next.removeFromList(key);
                height--;
                return this;
            } else {
                throw new KeyNotPresentException("Specified to be removed key do not exist on the list");
            }
        }
    }

    /**
     * Проверяет наличие заданного ключа в списке, начинающегося с данного.
     *
     * @param key
     *            - искомый ключ
     * @return - true, если ключ хранится в одном из узлов списка, false в
     *         противном случае
     */
    public boolean containsKey(K key) {
        if (pair.hasKey(key)) {
            return true;
        }
        if (next != null) {
            return next.containsKey(key);
        } else {
            return false;
        }
    }

    /**
     * Проверяет наличие заданной пары ключ-значение в списке, начинающегося с
     * данного.
     *
     * @param pair
     *            - искоммая пара ключ-значение
     * @return - true, если пара хранится в одном из узлов списка, false в
     *         противном случае
     */
    public boolean containsPair(KeyValuePair<K, V> pair) {
        if (this.pair.equals(pair)) {
            return true;
        }
        if (next != null) {
            return next.containsPair(pair);
        } else {
            return false;
        }
    }

    /**
     * Заменяет значение, хранимое в узле с заданным ключом
     *
     * @param key
     *            - ключ изменяемого узла
     * @param value
     *            - новое значение
     * @throws KeyNotPresentException
     *             - выбрасывается, если в списке нет узла с заданным ключем.
     */
    public void replaceValue(K key, V value) throws KeyNotPresentException {
        if (pair.hasKey(key)) {
            pair = new KeyValuePair<K, V>(key, value);
            return;
        }
        if (next != null) {
            next.replaceValue(key, value);
        } else {
            throw new KeyNotPresentException("Specified for value replacement key do not exist on the list");
        }
    }

    /**
     * Возвращает значение, хранимое в узле с заданным ключом
     *
     * @param key
     *            - ключ изменяемого узла
     * @throws KeyNotPresentException
     *             - выбрасывается, если в списке нет узла с заданным ключем.
     */
    public Object getValue(K key) throws KeyNotPresentException {
        if (pair.hasKey(key)) {
            return pair.getValue();
        }
        if (next != null) {
            return next.getValue(key);
        } else {
            throw new KeyNotPresentException("Specified to be retrieved key do not exist on the list");
        }
    }

    /**
     * Возвращает массив всех пар ключ-значение, хранимых в списке, начинающемся
     * с данного узла
     *
     * @return - массив пар ключ-значение, минимум 1 пара.
     */
    public KeyValuePair<K, V>[] getKeyValuePairs() {
        if (next != null) {
            KeyValuePair<K, V>[] nextPairs = next.getKeyValuePairs();
            KeyValuePair<K, V>[] pairs = Arrays.copyOf(nextPairs, nextPairs.length + 1);
            pairs[pairs.length - 1] = pair;
            return pairs;
        } else {
            KeyValuePair<K, V>[] pairs = new KeyValuePair[1];
            pairs[0] = pair;
            return pairs;
        }
    }

    /**
     * Возвращает массив хэшей всех пар ключ-значение, хранимых в списке,
     * начинающемся с данного узла
     *
     * @return - массив хэшей пар ключ-значение, минимум 1 хэш.
     */
    public int[] getKeyValuePairsHashes() {
        final KeyValuePair<K, V>[] pairs = getKeyValuePairs();
        int[] hashes = new int[pairs.length];
        for (int i = 0; i < pairs.length; i++) {
            hashes[i] = pairs[i].hashCode();
        }
        return hashes;
    }

    /**
     * Добавляет описание (ключ и значение) этого элемента и всех следующих за
     * ним в StringBuilder
     *
     * @param line
     *            - StringBuilder, в который следует добавить описания.
     */
    public void describeList(StringBuilder line) {
        pair.describeSelf(line);
        if (this.next != null) {
            line.append(",");
            this.next.describeList(line);
        }
    }

    @Override
    public String toString() {
        final StringBuilder line = new StringBuilder();
        describeList(line);
        return "List{" + line + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || this.getClass() != o.getClass())
            return false;
        final Node<K, V> node = (Node<K, V>) o;
        if (this.height != node.height) { 
            return false;
        }
        return isSubListOf(node); 
    }

    /**
     * Проверяет является ли логически данный список подмножеством другого: т.
     * е. входят ли все элементы данного списка и в другой тоже.
     * 
     * @param node
     *            - голова другого списка
     * @return - true, если все элементы данного списка входят в другой, false в
     *         обратном случае
     */
    private boolean isSubListOf(Node<K, V> node) {
        final KeyValuePair<K, V>[] pairs = getKeyValuePairs();
        for (KeyValuePair<K, V> checkedPair : pairs) {
            if (!node.containsPair(checkedPair)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int[] hashes = getKeyValuePairsHashes();
        Arrays.sort(hashes);
        return Arrays.hashCode(hashes);
    }
}
